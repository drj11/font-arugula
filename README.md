# Arugula

Sourced from a couple of photographs of stencilling on a
repurposed ammunition carrier for a rocket,
which featured the word «ROCKET».

The name is a pun,
the edible plant _Rocket_ is also known as _Arugula_.

See also VTG STENCIL UK 76 (Andreas Seidel).
The **G** and **S** are similar enough in style that some sort
of common ancestral stencil source seems likely.
Although other letters suggest that the stencils can't have been
the same (**E** **K**).


## Blurb

_Arugula_ is a military-style stencil sans-serif in
quite narrow proportions.
It is developed from found stencilling on
the side of a case of rocket ammunition.

At more than 150 glyphs, the repertoire is enough to support
Welsh, Slovene, and basic English.
Accents in the font can be placed onto letters and numbers
using the Unicode combining feature.

For wayfinding, arrows are supplied, and markers for
croissant, coffee, and fountain.


## Language notes

Capital letters only, no lower case.

Slovenian and Welsh are supported.
Basic English is supported.

Any accent can be applied to any letter (using the OpenType
`mark` feature).
The _macron_ has been provided as a fairly neutral place-holder
accent, but also means Nahuatl is supported.

Users who feel the lack of Ñ may consider if
N̄ can be used instead.


## Technical notes

Numbers also have `top` and `bottom` anchors (as well as
letters, mentioned in the language notes).
This supports adding marks, such as u+0304 COMBINING MACRON.


## 2023 Metrics

A redraw in 2023 is designed to close-up the gap between parts
of the same glyph, and increase the spacing between letters.
Both in order to increase perceived clarity.

I suggest the following metrics:

- Cap Height: 2800 (70% UPM)
- Stem Width: 450
- Stroke Height: 430
- Gap: 180 (40% of stem)


## Design Notes

There is a lean on **A** which might be a camera perspective
effect.

## END
